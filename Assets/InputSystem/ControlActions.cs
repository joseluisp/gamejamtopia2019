// GENERATED AUTOMATICALLY FROM 'Assets/InputSystem/ControlActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class ControlActions : IInputActionCollection
{
    private InputActionAsset asset;
    public ControlActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""ControlActions"",
    ""maps"": [
        {
            ""name"": ""Menu"",
            ""id"": ""9ef5dadc-b155-4c2e-9919-7e1a861cd6c8"",
            ""actions"": [
                {
                    ""name"": ""Dpad"",
                    ""id"": ""86e9f35d-d121-44c5-b511-3595bfd7e655"",
                    ""expectedControlLayout"": ""Dpad"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""LeftStick"",
                    ""id"": ""31520df5-4b48-4b71-92df-c37a287f2916"",
                    ""expectedControlLayout"": ""Stick"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""RightStick"",
                    ""id"": ""be3e6dba-1b52-4d18-95b3-c5a2609ada1f"",
                    ""expectedControlLayout"": ""Stick"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""Buttons"",
                    ""id"": ""ed8a0c3e-d2ec-43eb-b9e6-adc398379562"",
                    ""expectedControlLayout"": ""Button"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""ExitButton"",
                    ""id"": ""0370abcd-1d33-4c32-b906-3c7bf0c7d749"",
                    ""expectedControlLayout"": ""Button"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""MovementMouse"",
                    ""id"": ""9dd786aa-20f6-492f-a20a-0c2beeece4e6"",
                    ""expectedControlLayout"": """",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""0ab79d70-ba95-4b54-a6a5-e0aebaca7af9"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dpad"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""up"",
                    ""id"": ""fdccc7c2-f777-4d9c-a0b0-775d2cb5eb4d"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dpad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""down"",
                    ""id"": ""08737301-5290-4e68-9d4b-2f770bc3ec4f"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dpad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""left"",
                    ""id"": ""6d453ee5-b6a0-45de-9940-761a542d6950"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dpad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""right"",
                    ""id"": ""e3541b79-6ba1-4afb-8900-0e369ef73cb2"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Dpad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""6c63aa34-5db9-426b-96cf-4423f1ea3b15"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""c1931e1d-ae65-43df-8157-b39cbd9fe46f"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""3f83f858-f0f3-4c29-91c1-f0cc54f1f1a2"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Buttons"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""491ecf79-6acc-4ac3-b37c-28e68ef87803"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ExitButton"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""a4872587-151a-4a17-9a89-f6d58e36a961"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MovementMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                }
            ]
        },
        {
            ""name"": ""Scenes"",
            ""id"": ""6323005c-a85d-4b19-9cab-2743ee7cb713"",
            ""actions"": [
                {
                    ""name"": ""DPad"",
                    ""id"": ""a384086e-2db0-4f72-920c-393b191a53e8"",
                    ""expectedControlLayout"": """",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""RightStick"",
                    ""id"": ""4b6cf995-5461-4a3e-8095-07ba532033c4"",
                    ""expectedControlLayout"": ""Stick"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""LeftStick"",
                    ""id"": ""f5362ca3-4c4c-478c-b16f-8d206b925c17"",
                    ""expectedControlLayout"": ""Stick"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""Buttons"",
                    ""id"": ""bf8a0d35-22b4-415f-b738-bcd2c6d439f3"",
                    ""expectedControlLayout"": ""Button"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""KeyUp"",
                    ""id"": ""2de08ea4-0083-4405-82b4-b95babdbe26b"",
                    ""expectedControlLayout"": ""AnyKey"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""KeysDown"",
                    ""id"": ""696924ef-4645-48ea-b925-d319ca0306f6"",
                    ""expectedControlLayout"": ""AnyKey"",
                    ""continuous"": false,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""KeysRight"",
                    ""id"": ""d9a7be7d-cb8d-4cad-9dce-f98b6d20620f"",
                    ""expectedControlLayout"": ""AnyKey"",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""KeysLeft"",
                    ""id"": ""b92829cd-cb68-4c77-9f55-c892953ce3a8"",
                    ""expectedControlLayout"": ""AnyKey"",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""QHold"",
                    ""id"": ""f839d5b2-33e0-43de-a758-b90c7338ab8a"",
                    ""expectedControlLayout"": ""Key"",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""WHold"",
                    ""id"": ""4b59a7cd-71d6-403a-b4c8-57209bc3a07f"",
                    ""expectedControlLayout"": ""Key"",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""EHold"",
                    ""id"": ""5843c59b-b0d4-4726-adf2-9485663bd13e"",
                    ""expectedControlLayout"": ""Key"",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                },
                {
                    ""name"": ""RHold"",
                    ""id"": ""65ee1261-beb0-4848-b1c5-f31636d27786"",
                    ""expectedControlLayout"": ""Key"",
                    ""continuous"": true,
                    ""passThrough"": false,
                    ""initialStateCheck"": false,
                    ""processors"": """",
                    ""interactions"": """",
                    ""bindings"": []
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""b7586f73-2ebc-429e-8c62-542c9c92c2d5"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DPad"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""up"",
                    ""id"": ""8ae05126-b74a-4711-8454-2fd4a67ba4a5"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DPad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""down"",
                    ""id"": ""cb44835d-461d-48ed-ba7c-6195bde0351f"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DPad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c51dd568-dcac-4928-b934-7416d54fca16"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DPad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": ""right"",
                    ""id"": ""4ee46091-6196-437b-83e9-b04db0b992b5"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DPad"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""1c8e8a82-7a64-4a0a-ab9a-611b3e131747"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""7dfc15fe-7704-4b60-b66a-003d3a0ac75e"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightStick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""6a37fba4-291e-4dac-b0dd-936c3ab03ca6"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Buttons"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""6f0cbb24-446c-4e8e-9cba-043702fe4bcf"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""KeyUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""b58354ac-1752-49ed-816a-1e72f78f45fa"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""KeysDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""dabedf87-72f2-4f23-9d58-6c8fc027b42b"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""KeysRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""be5c0e21-0ee6-46f5-a530-f4f24cd15c51"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""KeysLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""3d621701-97e3-413e-bf72-f1ea738096f5"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": ""Hold(duration=0.15,pressPoint=0.1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""QHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""69e779df-d7e9-4e26-b44f-92df580d92f9"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": ""Hold(duration=0.15,pressPoint=0.1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""WHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""6cde0d3c-7f6b-4718-a253-de7e73b1e67c"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": ""Hold(duration=0.15,pressPoint=0.1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""EHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                },
                {
                    ""name"": """",
                    ""id"": ""7f497924-d8c6-4d85-922d-2ad88e84446e"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": ""Hold(duration=0.15,pressPoint=0.1)"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RHold"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false,
                    ""modifiers"": """"
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Menu
        m_Menu = asset.GetActionMap("Menu");
        m_Menu_Dpad = m_Menu.GetAction("Dpad");
        m_Menu_LeftStick = m_Menu.GetAction("LeftStick");
        m_Menu_RightStick = m_Menu.GetAction("RightStick");
        m_Menu_Buttons = m_Menu.GetAction("Buttons");
        m_Menu_ExitButton = m_Menu.GetAction("ExitButton");
        m_Menu_MovementMouse = m_Menu.GetAction("MovementMouse");
        // Scenes
        m_Scenes = asset.GetActionMap("Scenes");
        m_Scenes_DPad = m_Scenes.GetAction("DPad");
        m_Scenes_RightStick = m_Scenes.GetAction("RightStick");
        m_Scenes_LeftStick = m_Scenes.GetAction("LeftStick");
        m_Scenes_Buttons = m_Scenes.GetAction("Buttons");
        m_Scenes_KeyUp = m_Scenes.GetAction("KeyUp");
        m_Scenes_KeysDown = m_Scenes.GetAction("KeysDown");
        m_Scenes_KeysRight = m_Scenes.GetAction("KeysRight");
        m_Scenes_KeysLeft = m_Scenes.GetAction("KeysLeft");
        m_Scenes_QHold = m_Scenes.GetAction("QHold");
        m_Scenes_WHold = m_Scenes.GetAction("WHold");
        m_Scenes_EHold = m_Scenes.GetAction("EHold");
        m_Scenes_RHold = m_Scenes.GetAction("RHold");
    }

    ~ControlActions()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes
    {
        get => asset.controlSchemes;
    }

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Menu
    private InputActionMap m_Menu;
    private IMenuActions m_MenuActionsCallbackInterface;
    private InputAction m_Menu_Dpad;
    private InputAction m_Menu_LeftStick;
    private InputAction m_Menu_RightStick;
    private InputAction m_Menu_Buttons;
    private InputAction m_Menu_ExitButton;
    private InputAction m_Menu_MovementMouse;
    public struct MenuActions
    {
        private ControlActions m_Wrapper;
        public MenuActions(ControlActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Dpad { get { return m_Wrapper.m_Menu_Dpad; } }
        public InputAction @LeftStick { get { return m_Wrapper.m_Menu_LeftStick; } }
        public InputAction @RightStick { get { return m_Wrapper.m_Menu_RightStick; } }
        public InputAction @Buttons { get { return m_Wrapper.m_Menu_Buttons; } }
        public InputAction @ExitButton { get { return m_Wrapper.m_Menu_ExitButton; } }
        public InputAction @MovementMouse { get { return m_Wrapper.m_Menu_MovementMouse; } }
        public InputActionMap Get() { return m_Wrapper.m_Menu; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled { get { return Get().enabled; } }
        public InputActionMap Clone() { return Get().Clone(); }
        public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
        public void SetCallbacks(IMenuActions instance)
        {
            if (m_Wrapper.m_MenuActionsCallbackInterface != null)
            {
                Dpad.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnDpad;
                Dpad.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnDpad;
                Dpad.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnDpad;
                LeftStick.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeftStick;
                LeftStick.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeftStick;
                LeftStick.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnLeftStick;
                RightStick.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnRightStick;
                RightStick.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnRightStick;
                RightStick.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnRightStick;
                Buttons.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnButtons;
                Buttons.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnButtons;
                Buttons.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnButtons;
                ExitButton.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnExitButton;
                ExitButton.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnExitButton;
                ExitButton.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnExitButton;
                MovementMouse.started -= m_Wrapper.m_MenuActionsCallbackInterface.OnMovementMouse;
                MovementMouse.performed -= m_Wrapper.m_MenuActionsCallbackInterface.OnMovementMouse;
                MovementMouse.canceled -= m_Wrapper.m_MenuActionsCallbackInterface.OnMovementMouse;
            }
            m_Wrapper.m_MenuActionsCallbackInterface = instance;
            if (instance != null)
            {
                Dpad.started += instance.OnDpad;
                Dpad.performed += instance.OnDpad;
                Dpad.canceled += instance.OnDpad;
                LeftStick.started += instance.OnLeftStick;
                LeftStick.performed += instance.OnLeftStick;
                LeftStick.canceled += instance.OnLeftStick;
                RightStick.started += instance.OnRightStick;
                RightStick.performed += instance.OnRightStick;
                RightStick.canceled += instance.OnRightStick;
                Buttons.started += instance.OnButtons;
                Buttons.performed += instance.OnButtons;
                Buttons.canceled += instance.OnButtons;
                ExitButton.started += instance.OnExitButton;
                ExitButton.performed += instance.OnExitButton;
                ExitButton.canceled += instance.OnExitButton;
                MovementMouse.started += instance.OnMovementMouse;
                MovementMouse.performed += instance.OnMovementMouse;
                MovementMouse.canceled += instance.OnMovementMouse;
            }
        }
    }
    public MenuActions @Menu
    {
        get
        {
            return new MenuActions(this);
        }
    }

    // Scenes
    private InputActionMap m_Scenes;
    private IScenesActions m_ScenesActionsCallbackInterface;
    private InputAction m_Scenes_DPad;
    private InputAction m_Scenes_RightStick;
    private InputAction m_Scenes_LeftStick;
    private InputAction m_Scenes_Buttons;
    private InputAction m_Scenes_KeyUp;
    private InputAction m_Scenes_KeysDown;
    private InputAction m_Scenes_KeysRight;
    private InputAction m_Scenes_KeysLeft;
    private InputAction m_Scenes_QHold;
    private InputAction m_Scenes_WHold;
    private InputAction m_Scenes_EHold;
    private InputAction m_Scenes_RHold;
    public struct ScenesActions
    {
        private ControlActions m_Wrapper;
        public ScenesActions(ControlActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @DPad { get { return m_Wrapper.m_Scenes_DPad; } }
        public InputAction @RightStick { get { return m_Wrapper.m_Scenes_RightStick; } }
        public InputAction @LeftStick { get { return m_Wrapper.m_Scenes_LeftStick; } }
        public InputAction @Buttons { get { return m_Wrapper.m_Scenes_Buttons; } }
        public InputAction @KeyUp { get { return m_Wrapper.m_Scenes_KeyUp; } }
        public InputAction @KeysDown { get { return m_Wrapper.m_Scenes_KeysDown; } }
        public InputAction @KeysRight { get { return m_Wrapper.m_Scenes_KeysRight; } }
        public InputAction @KeysLeft { get { return m_Wrapper.m_Scenes_KeysLeft; } }
        public InputAction @QHold { get { return m_Wrapper.m_Scenes_QHold; } }
        public InputAction @WHold { get { return m_Wrapper.m_Scenes_WHold; } }
        public InputAction @EHold { get { return m_Wrapper.m_Scenes_EHold; } }
        public InputAction @RHold { get { return m_Wrapper.m_Scenes_RHold; } }
        public InputActionMap Get() { return m_Wrapper.m_Scenes; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled { get { return Get().enabled; } }
        public InputActionMap Clone() { return Get().Clone(); }
        public static implicit operator InputActionMap(ScenesActions set) { return set.Get(); }
        public void SetCallbacks(IScenesActions instance)
        {
            if (m_Wrapper.m_ScenesActionsCallbackInterface != null)
            {
                DPad.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnDPad;
                DPad.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnDPad;
                DPad.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnDPad;
                RightStick.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnRightStick;
                RightStick.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnRightStick;
                RightStick.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnRightStick;
                LeftStick.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnLeftStick;
                LeftStick.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnLeftStick;
                LeftStick.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnLeftStick;
                Buttons.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnButtons;
                Buttons.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnButtons;
                Buttons.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnButtons;
                KeyUp.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeyUp;
                KeyUp.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeyUp;
                KeyUp.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeyUp;
                KeysDown.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysDown;
                KeysDown.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysDown;
                KeysDown.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysDown;
                KeysRight.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysRight;
                KeysRight.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysRight;
                KeysRight.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysRight;
                KeysLeft.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysLeft;
                KeysLeft.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysLeft;
                KeysLeft.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnKeysLeft;
                QHold.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnQHold;
                QHold.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnQHold;
                QHold.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnQHold;
                WHold.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnWHold;
                WHold.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnWHold;
                WHold.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnWHold;
                EHold.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnEHold;
                EHold.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnEHold;
                EHold.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnEHold;
                RHold.started -= m_Wrapper.m_ScenesActionsCallbackInterface.OnRHold;
                RHold.performed -= m_Wrapper.m_ScenesActionsCallbackInterface.OnRHold;
                RHold.canceled -= m_Wrapper.m_ScenesActionsCallbackInterface.OnRHold;
            }
            m_Wrapper.m_ScenesActionsCallbackInterface = instance;
            if (instance != null)
            {
                DPad.started += instance.OnDPad;
                DPad.performed += instance.OnDPad;
                DPad.canceled += instance.OnDPad;
                RightStick.started += instance.OnRightStick;
                RightStick.performed += instance.OnRightStick;
                RightStick.canceled += instance.OnRightStick;
                LeftStick.started += instance.OnLeftStick;
                LeftStick.performed += instance.OnLeftStick;
                LeftStick.canceled += instance.OnLeftStick;
                Buttons.started += instance.OnButtons;
                Buttons.performed += instance.OnButtons;
                Buttons.canceled += instance.OnButtons;
                KeyUp.started += instance.OnKeyUp;
                KeyUp.performed += instance.OnKeyUp;
                KeyUp.canceled += instance.OnKeyUp;
                KeysDown.started += instance.OnKeysDown;
                KeysDown.performed += instance.OnKeysDown;
                KeysDown.canceled += instance.OnKeysDown;
                KeysRight.started += instance.OnKeysRight;
                KeysRight.performed += instance.OnKeysRight;
                KeysRight.canceled += instance.OnKeysRight;
                KeysLeft.started += instance.OnKeysLeft;
                KeysLeft.performed += instance.OnKeysLeft;
                KeysLeft.canceled += instance.OnKeysLeft;
                QHold.started += instance.OnQHold;
                QHold.performed += instance.OnQHold;
                QHold.canceled += instance.OnQHold;
                WHold.started += instance.OnWHold;
                WHold.performed += instance.OnWHold;
                WHold.canceled += instance.OnWHold;
                EHold.started += instance.OnEHold;
                EHold.performed += instance.OnEHold;
                EHold.canceled += instance.OnEHold;
                RHold.started += instance.OnRHold;
                RHold.performed += instance.OnRHold;
                RHold.canceled += instance.OnRHold;
            }
        }
    }
    public ScenesActions @Scenes
    {
        get
        {
            return new ScenesActions(this);
        }
    }
    public interface IMenuActions
    {
        void OnDpad(InputAction.CallbackContext context);
        void OnLeftStick(InputAction.CallbackContext context);
        void OnRightStick(InputAction.CallbackContext context);
        void OnButtons(InputAction.CallbackContext context);
        void OnExitButton(InputAction.CallbackContext context);
        void OnMovementMouse(InputAction.CallbackContext context);
    }
    public interface IScenesActions
    {
        void OnDPad(InputAction.CallbackContext context);
        void OnRightStick(InputAction.CallbackContext context);
        void OnLeftStick(InputAction.CallbackContext context);
        void OnButtons(InputAction.CallbackContext context);
        void OnKeyUp(InputAction.CallbackContext context);
        void OnKeysDown(InputAction.CallbackContext context);
        void OnKeysRight(InputAction.CallbackContext context);
        void OnKeysLeft(InputAction.CallbackContext context);
        void OnQHold(InputAction.CallbackContext context);
        void OnWHold(InputAction.CallbackContext context);
        void OnEHold(InputAction.CallbackContext context);
        void OnRHold(InputAction.CallbackContext context);
    }
}
