﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ButtonController : MonoBehaviour
{


    public ControlActions inputActions;

    [SerializeField] private SpriteRenderer buttonNormal = null;
    //[SerializeField] private Sprite buttonNormal = null;
    [SerializeField] private SpriteRenderer buttonPressed = null;

    [SerializeField] private KeyCode keyToPress = KeyCode.None;
    [SerializeField] private GamePlayController gamePlayController = null;
    [SerializeField] private SpriteRenderer spriterend = null;
    [SerializeField] private Color colorPressed = Color.white;
    [SerializeField] private Color colorNormal = Color.white;

    //private float startTime = 0f;
    [HideInInspector] public float timer = 0f;
    //private float holdTime = 0.1f;
    [HideInInspector] public bool keyHolded = false;
    [SerializeField] public bool keyCanBeHolded = false;
    public bool keyNeedUp = false;
    public bool inHoldCollision = false;
    public char letra = '*';

    private void OnEnable()
    {

        if (inputActions != null)
        {

            inputActions.Enable();

        }

    }

    private void OnDisable()
    {
        if (inputActions != null)
        {
            inputActions.Disable();

        }

    }


    private void Awake()
    {

        //buttonNormal.enabled = true;
        //buttonPressed.enabled = false;

        inputActions = new ControlActions();
        inputActions.Scenes.QHold.performed += QHold;
        inputActions.Scenes.QHold.canceled += QHoldCanceled;

        inputActions.Scenes.WHold.performed += WHold;
        inputActions.Scenes.WHold.canceled += WHoldCanceled;

        inputActions.Scenes.EHold.performed += EHold;
        inputActions.Scenes.EHold.canceled += EHoldCanceled;

        inputActions.Scenes.RHold.performed += RHold;
        inputActions.Scenes.RHold.canceled += RHoldCanceled;



    }

    private void QHoldCanceled(InputAction.CallbackContext obj)
    {


        if (obj.control.name != "q") return;

        switch (keyToPress)
        {

            case KeyCode.Q:
                gamePlayController.fuegoHold[0].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
                keyNeedUp = false;
                keyHolded = false;
                spriterend.color = colorNormal;


                break;
        }

    }

    private void QHold(InputAction.CallbackContext obj)
    {

        

        if (obj.control.name != "q") return;

        if (inHoldCollision == true)
        {

            //print("keyCanBeHolded=" + keyCanBeHolded + " keyneedup=" + keyNeedUp);
            if (keyCanBeHolded == true)
            {

                if (keyNeedUp == true) //no se llama
                {
                    //print("soltado");




                    switch (keyToPress)
                    {

                        case KeyCode.Q:
                            gamePlayController.fuegoHold[0].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
                            break;
                    }



                }
                else
                {

                    if (keyHolded == false)
                    {
                        switch (keyToPress)
                        {

                            case KeyCode.Q:

                                gamePlayController.fuegoHold[0].Play();
                                gamePlayController.PlayerPrincipioHold(150);
                                keyHolded = true;
                                spriterend.color = colorPressed;

                                break;
                        }

                    }
                   

                }




            }
            else
            {

                print("cannotbeholdedd"); //tampoco se llama
            }




        }
        else
        {

            if (keyNeedUp == true)
            {

                switch (keyToPress)
                {

                    case KeyCode.Q: gamePlayController.fuegoHold[0].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                }

            }



        }





    }


    //no template
    private void WHoldCanceled(InputAction.CallbackContext obj)
    {
        
        if (obj.control.name != "w") return;


        switch (keyToPress)
        {

            case KeyCode.W:
                keyNeedUp = false;
                gamePlayController.fuegoHold[1].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
                keyHolded = false;
                spriterend.color = colorNormal;
                break;
        }

    }

    private void WHold(InputAction.CallbackContext obj)
    {


        if (obj.control.name != "w") return;

        if (inHoldCollision == true)
        {

            if (keyCanBeHolded == true)
            {

                if (keyNeedUp == true)
                {

                    switch (keyToPress)
                    {

                        case KeyCode.W: gamePlayController.fuegoHold[1].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                    }



                }
                else
                {


                    if (keyHolded == false)
                    {
                        switch (keyToPress)
                        {

                            case KeyCode.W:
                                gamePlayController.PlayerPrincipioHold(150);
                                gamePlayController.fuegoHold[1].Play();
                                keyHolded = true;
                                spriterend.color = colorPressed;
                                break;
                        }
                    }

                }




            }
            else
            {

                print("cannotbeholdedd");
            }


        }
        else
        {

            if (keyNeedUp == true)
            {

                switch (keyToPress)
                {

                    case KeyCode.W: gamePlayController.fuegoHold[1].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                }

            }



        }

    }

    private void EHoldCanceled(InputAction.CallbackContext obj)
    {

        if (obj.control.name != "e") return;


        switch (keyToPress)
        {

            case KeyCode.E:
                keyNeedUp = false;
                gamePlayController.fuegoHold[2].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
                keyHolded = false;
                spriterend.color = colorNormal;
                break;
        }

    }

    private void EHold(InputAction.CallbackContext obj)
    {


        if (obj.control.name != "e") return;

        if (inHoldCollision == true)
        {

            if (keyCanBeHolded == true)
            {

                if (keyNeedUp == true)
                {




                    switch (keyToPress)
                    {

                        case KeyCode.E: gamePlayController.fuegoHold[2].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                    }



                }
                else
                {

                    if (keyHolded == false)
                    {


                        switch (keyToPress)
                        {

                            case KeyCode.E:
                                gamePlayController.PlayerPrincipioHold(150);
                                gamePlayController.fuegoHold[2].Play();
                                keyHolded = true;
                                spriterend.color = colorPressed;

                                break;
                        }

                    }

                }




            }
            else
            {

                print("cannotbeholdedd");
            }


        }
        else
        {

            if (keyNeedUp == true)
            {

                switch (keyToPress)
                {

                    case KeyCode.E: gamePlayController.fuegoHold[2].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                }

            }



        }

    }

    private void RHoldCanceled(InputAction.CallbackContext obj)
    {
        if (obj.control.name != "r") return;


        switch (keyToPress)
        {

            case KeyCode.R:

                keyNeedUp = false;
                keyHolded = false;
                gamePlayController.fuegoHold[3].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
                spriterend.color = colorNormal;

                break;
        }

    }

    private void RHold(InputAction.CallbackContext obj)
    {

        //Key:/ Keyboard / q
        //print("" + obj.control.name); //q

        if (obj.control.name != "r") return;

        if (inHoldCollision == true)
        {

            //print("keyCanBeHolded=" + keyCanBeHolded + " keyneedup=" + keyNeedUp);
            if (keyCanBeHolded == true)
            {

                if (keyNeedUp == true)
                {
                    //print("soltado");




                    switch (keyToPress)
                    {

                        case KeyCode.R: gamePlayController.fuegoHold[3].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                    }



                }
                else
                {


                    if (keyHolded == false)
                    {

                        switch (keyToPress)
                        {

                            case KeyCode.R:
                                gamePlayController.PlayerPrincipioHold(150);
                                gamePlayController.fuegoHold[3].Play();
                                keyHolded = true;
                                spriterend.color = colorPressed;
                                break;
                        }

                    }

                }




            }
            else
            {

                print("cannotbeholdedd");
            }


        }
        else
        {

            if (keyNeedUp == true)
            {

                switch (keyToPress)
                {

                    case KeyCode.R: gamePlayController.fuegoHold[3].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                }

            }



        }






    }









    // Start is called before the first frame update
    void Start()
    {
        keyHolded = false;
        keyNeedUp = false;
        keyCanBeHolded = false;

    }



    public IEnumerator SetColorToPress()
    {
        spriterend.color = colorPressed;
        yield return new WaitForSeconds(0.5f);
        spriterend.color = colorNormal;

        yield break;

    }


    public void SetColorToNormal()
    {

        spriterend.color = colorNormal;
    }


}
