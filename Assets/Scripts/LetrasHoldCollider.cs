﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetrasHoldCollider : MonoBehaviour
{


    [SerializeField] private KeyCode keyNeededTobePressed = KeyCode.None;
    [SerializeField] public char letra = '0';
    [SerializeField] public GamePlayController gamePlayController = null;
    [SerializeField] public ButtonController buttonController = null;
    private ushort fases = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        //botones
        if (other.tag == "Botones")
        {
            //print("enter");

            buttonController.inHoldCollision = true;
            buttonController.keyCanBeHolded = true;
            buttonController.letra = letra;


            gamePlayController.EnemyPrincipioHold(150);

            fases = 1;


        }


        //Zona DESTROY
        if (other.tag == "DestroyBig")
        {

            //print("letra=" + letra + " fases="  + fases);
            if ((letra != 'S' || letra != '#' || letra != '*') && fases != 2)
            {

                //print("dejado pasar");
                gamePlayController.DejadoPasar();

            }

            Destroy(this.gameObject);

        }


    }

    private void OnTriggerStay2D(Collider2D other)
    {

        if (buttonController.keyHolded == true)
        {

            fases = 2;

        }
        else
        {
            fases = 0;

        }


    }

    private void OnTriggerExit2D(Collider2D other)
    {



        if (other.tag == "Botones")
        {

            //print("keyholed=" + buttonController.keyHolded + " fases=" + fases);
            if (buttonController.keyHolded == true && fases == 2)
            {

                //print("acierto");
                switch (keyNeededTobePressed)
                {

                    case KeyCode.Q: gamePlayController.PlayerCompleteHold(0, 150); break;
                    case KeyCode.W: gamePlayController.PlayerCompleteHold(1, 150); break;
                    case KeyCode.E: gamePlayController.PlayerCompleteHold(2, 150); break;
                    case KeyCode.R: gamePlayController.PlayerCompleteHold(3, 150); break;



                }

            }

            gamePlayController.EnemyCompleteHold(150);

            buttonController.inHoldCollision = false;
            buttonController.keyCanBeHolded = false;
            buttonController.keyHolded = false;

            buttonController.keyNeedUp = true;

            switch (letra)
            {

                case 'E': gamePlayController.fuegoHold[0].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                case 'F': gamePlayController.fuegoHold[1].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                case 'G': gamePlayController.fuegoHold[2].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;
                case 'H': gamePlayController.fuegoHold[3].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear); break;

            }


        }


    }






}
