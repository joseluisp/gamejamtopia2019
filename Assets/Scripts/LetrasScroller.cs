﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetrasScroller : MonoBehaviour
{

    [SerializeField] public float tempoMusica = 0;
    [SerializeField] public GamePlayController gamePlayController = null;
    

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {

        if (gamePlayController.started == false) return;

        this.transform.position -= new Vector3(0, tempoMusica * Time.deltaTime, 0);


    }
}
