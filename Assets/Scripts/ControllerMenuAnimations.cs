﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerMenuAnimations : MonoBehaviour
{


    [SerializeField] private Animation animations = null;

    // Start is called before the first frame update
    void Start()
    {
        animations.Play("Titulo");
    }



    public void DesactiveMenu()
    {

        animations.Play("MenuDesactivate");

    }


}
