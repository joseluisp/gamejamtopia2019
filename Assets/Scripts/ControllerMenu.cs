﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UniRx.Async;

public class ControllerMenu : MonoBehaviour
{


    
    public GamePlayController gamePlayController = null;
    public MusicController musicController = null;

    public ParticleSystem logoParticle = null;

    public GameObject selectLorca;
    public GameObject selectShakes;

    public GameObject[] paneles;

    public Animation animationMenu = null;
    public string playerName = "lorca";


    private bool isMutedDefault = false;
    private ushort mainVolumenDefault = 100;
    private ushort mainSoundDefault = 50;
    private ushort mainSfxDefault = 60;

    private bool isMutedInternal = false;
    private ushort mainVolumenInternal = 100;
    private ushort mainSoundInternal = 50;
    private ushort mainSfxInternal = 60;


    public Animation animSelection = null;
    public ParticleSystem[] particleSelection = null;
    public RectTransform particleSelectionTransform = null;

    public GameObject[] canvas = null;

    private async void Start()
    {

        

        //if (musicController == null)
        //{
        //    Debug.LogError("CARGAR DESDE LOADING");
        //    return;

        //}

        

        musicController.SettingsMixer(
           isMutedInternal,
           mainVolumenInternal,
           mainSoundInternal,
           mainSfxInternal
       );

        

        await UniTask.Delay(2000);

        musicController.PlayInGameMusic(musicController.sounds[0], true);


        canvas[0].SetActive(true);
        


        logoParticle.Play();

        await UniTask.Delay(3000);

        

        canvas[0].SetActive(false);
        canvas[1].SetActive(true);



        ShowInicio();

    }

    private void Awake()
    {


        DesactivarCanvas();
        DesactivarPaneles();
        
        GettingPlayerPrefsValues();

        selectLorca.SetActive(false);
        selectShakes.SetActive(false);

        isMutedInternal = false;


        canvas[5].gameObject.SetActive(false);

        


    }

    private void DesactivarPaneles()
    {

        for (ushort i = 0; i < paneles.Length; i++)
        {
            paneles[i].SetActive(false);

        }

    }


    public void DesactivarCanvas()
    {

        for (ushort i = 0; i < canvas.Length; i++)
        {


            canvas[i].SetActive(false);
        }


    }

    


    public async void GoToSelectPersonaje()
    {

        DesactivarCanvas();

        canvas[3].SetActive(true);

        selectLorca.SetActive(true);
        selectShakes.SetActive(true);


        //print("fromretry=" + gamePlayController.fromRetry);
        if (gamePlayController.fromRetry == false)
        {

            animSelection.Play("AppearSeleccionPersonajes");

        }
        else
        {

            animSelection.Play("TransitionOpen");
            await UniTask.Delay((int)animSelection.GetClip("TransitionOpen").length * 1000); 
            //animSelection.Play("AppearSeleccionPersonajes");
        }




    }


    public void ShowOptions()
    {
        DesactivarCanvas();


        if (isMutedInternal == true)
        {
            textosOptions[0].text = "ON";
        }
        else
        {

            textosOptions[0].text = "OFF";


        }

        textosOptions[1].text = mainVolumenInternal.ToString();
        textosOptions[2].text = mainSoundInternal.ToString();
        textosOptions[3].text = mainSfxInternal.ToString();


        canvas[2].SetActive(true);




    }


    public void Credits()
    {

        DesactivarCanvas();

        canvas[4].SetActive(true);

    }

    public void Quit()
    {



#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#else


        Application.Quit();
#endif


    }


    public void ShowInicio()
    {


        DesactivarCanvas();

        canvas[1].SetActive(true);
        animationMenu.Play("MenuAppear");

    }

    private void GettingPlayerPrefsValues()
    {

        if (PlayerPrefs.HasKey("isMutedInternal") == true)
        {

            int t = PlayerPrefs.GetInt("isMutedInternal");


            if (t == 0)
            {

                isMutedInternal = false;
            }
            else if (t == 1)
            {

                isMutedInternal = true;

            }



        }
        else
        {

            if (isMutedDefault == true)
            {

                PlayerPrefs.SetInt("isMutedInternal", 1);

            }
            else
            {

                PlayerPrefs.SetInt("isMutedInternal", 0);

            }


        }

        if (PlayerPrefs.HasKey("mainVolumenInternal") == true)
        {

            mainVolumenInternal = (ushort)PlayerPrefs.GetInt("mainVolumenInternal");

        }
        else
        {

            PlayerPrefs.SetInt("mainVolumenInternal", mainVolumenDefault);
        }


        if (PlayerPrefs.HasKey("mainSoundInternal") == true)
        {

            mainSoundInternal = (ushort)PlayerPrefs.GetInt("mainSoundInternal");

        }
        else
        {

            PlayerPrefs.SetInt("mainSoundInternal", mainSoundDefault);
        }



        if (PlayerPrefs.HasKey("mainSfxInternal") == true)
        {

            mainSfxInternal = (ushort)PlayerPrefs.GetInt("mainSfxInternal");

        }
        else
        {

            PlayerPrefs.SetInt("mainSfxInternal", mainSfxDefault);
        }



    }


    private void SavePlayerPrefsValues()
    {

        if (isMutedInternal == true)
        {

            PlayerPrefs.SetInt("isMutedInternal", 1);

        }
        else
        {

            PlayerPrefs.SetInt("isMutedInternal", 0);

        }

        PlayerPrefs.SetInt("mainVolumenInternal", mainVolumenInternal);
        PlayerPrefs.SetInt("mainSfxInternal", mainSfxInternal);
        PlayerPrefs.SetInt("mainVolumenInternal", mainVolumenInternal);

    }

    [HideInInspector] public bool isPlayerSetted = false;

    public async void SetLorca()
    {
        if (isPlayerSetted == true) return;

        playerName = "lorca";

        particleSelection[1].Play();
        animSelection.Play("ClickedCharacterLorca");
        isPlayerSetted = true;
        await UniTask.Delay(300);
        animSelection.Play("TransitionClose");

        await UniTask.Delay(1000);

        gamePlayController.InitGame();

    }


    public async void SetShakes()
    {

        if (isPlayerSetted == true) return;

        isPlayerSetted = true;

        playerName = "shakes";
        particleSelection[0].Play();
        animSelection.Play("ClickedCharacterShakes");
        await UniTask.Delay(300);
        animSelection.Play("TransitionClose");

        await UniTask.Delay(1000);

        gamePlayController.InitGame();

    }


    public void Mute()
    {

        isMutedInternal = !isMutedInternal;

        musicController.PlayFXSound(
            musicController.sfx[1]
        );

        if (isMutedInternal == true)
        {
            textosOptions[0].text = "ON";
        }
        else
        {
            textosOptions[0].text = "OFF";
        }

        musicController.Mute();


    }


    public void MainVolumenMas()
    {


        if (mainVolumenInternal < 100)
        {

            mainVolumenInternal += 10;

            if (mainVolumenInternal >= 100)
            {

                mainVolumenInternal = 100;
            }

        }

        musicController.SetVolumenMaster(mainVolumenInternal);

        musicController.PlayFXSound(
            musicController.sfx[1]
            );

        textosOptions[1].text = mainVolumenInternal.ToString();


    }


    [SerializeField] private TextMeshProUGUI[] textosOptions = null;

    public void MainVolumenMenos()
    {


        if (mainVolumenInternal > 0)
        {

            mainVolumenInternal -= 10;

            if (mainVolumenInternal <= 0)
            {

                mainVolumenInternal = 0;
            }

        }

        musicController.SetVolumenMaster(mainVolumenInternal);

        musicController.PlayFXSound(
            musicController.sfx[1]
            );

        textosOptions[1].text = mainVolumenInternal.ToString();


    }


    public void MusicVolumentMas()
    {


        if (mainSoundInternal < 100)
        {

            mainSoundInternal += 10;

            if (mainSoundInternal > 100)
            {

                mainSoundInternal = 0;
            }

        }

        musicController.SetVolumenSounds(mainSoundInternal);
        musicController.PlayFXSound(
            musicController.sfx[1]
            );

        textosOptions[2].text = mainSoundInternal.ToString();


    }

    public void MusicVolumentMenos()
    {


        if (mainSoundInternal > 0)
        {

            mainSoundInternal -= 10;

            if (mainSoundInternal <= 0)
            {

                mainSoundInternal = 0;
            }

        }

        musicController.SetVolumenSounds(mainSoundInternal);

        musicController.PlayFXSound(
            musicController.sfx[1]
            );

        textosOptions[2].text = mainSoundInternal.ToString();


    }


    public void SfxVolumenMas()
    {

        if (mainSfxInternal < 100)
        {

            mainSfxInternal += 10;

            if (mainSfxInternal > 100)
            {

                mainSfxInternal = 0;
            }

        }

        musicController.SetVolumenSfx(mainSfxInternal);

        musicController.PlayFXSound(
            musicController.sfx[1]
            );

        textosOptions[3].text = mainSfxInternal.ToString();


    }

    public void SfxVolumentMenos()
    {


        if (mainSfxInternal > 0)
        {

            mainSfxInternal -= 10;

            if (mainSfxInternal <= 0)
            {

                mainSfxInternal = 0;
            }

        }

        musicController.SetVolumenSfx(mainSfxInternal);

        musicController.PlayFXSound(
            musicController.sfx[1]
            );

        textosOptions[3].text = mainSfxInternal.ToString();

    }


    public void ResetDefaultValues()
    {

        print("reset");

        isMutedInternal = isMutedDefault;
        mainVolumenInternal = mainVolumenDefault;
        mainSoundInternal = mainSoundDefault;
        mainSfxInternal = mainSfxDefault;

        if (isMutedInternal == true)
        {
            textosOptions[0].text = "ON";
        }
        else
        {

            textosOptions[0].text = "OFF";
        }



        musicController.PlayFXSound(
                   musicController.sfx[1]
               );

        musicController.MuteOff();

        musicController.SetVolumenMaster(mainVolumenInternal);
        musicController.SetVolumenSounds(mainSoundInternal);
        musicController.SetVolumenSfx(mainSfxInternal);

        textosOptions[1].text = mainVolumenInternal.ToString();
        textosOptions[2].text = mainSoundInternal.ToString();
        textosOptions[3].text = mainSfxInternal.ToString();



    }

    

    private void ShowFXSelectionPersonaje(float x, float y, ParticleSystem particle)
    {

        //delay para que se escuche el fx y pequeña transicion
        particleSelectionTransform.anchoredPosition = new Vector2(x, y);
        particle.Play();

    }


    public void OpenItchio()
    {
        Application.OpenURL("https://joseibanera.itch.io/versebattle");

    }


}
