﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControlMovement : MonoBehaviour
{



    public ControlActions inputActions;
    [SerializeField] private Animator animator = null;

    //private Vector3 playerPos = Vector3.zero;

    private void OnEnable()
    {

        inputActions.Enable();

    }

    private void OnDisable()
    {
        if (inputActions != null)
        {
            inputActions.Disable();

        }

    }

    private void Awake()
    {


        inputActions = new ControlActions();
        inputActions.Scenes.DPad.performed += PlayerDPADHandle;

        //inputActions.Menu.Buttons.performed += MenuButtonPressedGamepad;
        //inputActions.Menu.ExitButton.performed += ButtonExit;

        inputActions.Menu.LeftStick.performed += PlayerLeftStickHandle;
        //inputActions.Menu.LeftStick.canceled += MenuResetStickMove;

        //inputActions.Menu.RightStick.performed += MenuStickHandle;
        //inputActions.Menu.RightStick.canceled += MenuResetStickMove;

        //inputActions.Menu.MovementMouse.performed += MovementMouse;

        inputActions.Scenes.KeyUp.performed += KeyUpPressed;
        inputActions.Scenes.KeysDown.performed += KeyDownPressed;
        inputActions.Scenes.KeysLeft.performed += KeyLeftPressed;
        inputActions.Scenes.KeysRight.performed += KeyRightPressed;

        //inputActions.Scenes.KeyUp. += CancelMovement;
        //inputActions.Scenes.KeysDown.canceled += CancelMovement;
        //inputActions.Scenes.KeysLeft.canceled += CancelMovement;
        //inputActions.Scenes.KeysRight.canceled += CancelMovement;

        //playerPos = this.transform.position;


    }

    private void CancelMovement(InputAction.CallbackContext obj)
    {
        print("canceled");

        inputMovement = Vector2.zero;
    }

    private void KeyUpPressed(InputAction.CallbackContext obj)
    {


       
        
        print("up");

       



    }


    private void KeyDownPressed(InputAction.CallbackContext obj)
    {




        print("down");





    }



    private void KeyRightPressed(InputAction.CallbackContext obj)
    {


        //this.transform.position += new Vector3(1, 0, 0);

        //print("right");



        //inputMovement = Vector3.zero;
        inputMovement.x = 1;
        //inputMovement.z = Input.GetAxis("Vertical");

    }



    private void KeyLeftPressed(InputAction.CallbackContext obj)
    {



        //this.transform.position += new Vector3(-1, 0, 0);
        inputMovement.x = -1;
        //print("left");





    }





    // Start is called before the first frame update
    private void Start()
    {

        playerRigid = GetComponent<Rigidbody2D>();
        _groundChecker = transform.GetChild(0);

    }

   
    private void PlayerDPADHandle(InputAction.CallbackContext obj)
    {

    }

    private void PlayerLeftStickHandle(InputAction.CallbackContext obj)
    {


        var move = obj.ReadValue<Vector2>();
        print(move);

        if (move.x <= -0.5f)
        {
            print("izquierda");



        }
        else if (move.x >= 0.5f)
        {

            print("derecha");
        }

    }


    [SerializeField] private float Speed = 5f;
    //[SerializeField] private float JumpHeight = 2f;
    [SerializeField] private float GroundDistance = 0.2f;
    [SerializeField] private float DashDistance = 5f;
    [SerializeField] private LayerMask Ground;

    private Rigidbody2D playerRigid;
    private Vector2 inputMovement = Vector2.zero;
    private bool _isGrounded = true;
    private Transform _groundChecker;

 

    private void Update()
    {
        _isGrounded = Physics.CheckSphere(_groundChecker.position, GroundDistance, Ground, QueryTriggerInteraction.Ignore);


        //this.transform.rotation = Quaternion.identity;

        //if (inputMovement != Vector2.zero)
        //{

        //    this.transform.forward = inputMovement;

        //}

        //if (Input.GetButtonDown("Jump") && _isGrounded)
        //{
        //    playerRigid.AddForce(Vector3.up * Mathf.Sqrt(JumpHeight * -2f * Physics.gravity.y), ForceMode.VelocityChange);
        //}
        //if (Input.GetButtonDown("Dash"))
        //{
        //    Vector3 dashVelocity = Vector3.Scale(transform.forward, DashDistance * new Vector3((Mathf.Log(1f / (Time.deltaTime * playerRigid.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * playerRigid.drag + 1)) / -Time.deltaTime)));
        //    playerRigid.AddForce(dashVelocity, ForceMode.VelocityChange);
        //}
    }


    void FixedUpdate()
    {
        if (inputMovement != Vector2.zero)
        {
            //print("movimiento" + inputMovement);
            playerRigid.MovePosition(playerRigid.position + inputMovement * Speed * Time.fixedDeltaTime);
            inputMovement = Vector2.zero;
        }


       

    }



}
