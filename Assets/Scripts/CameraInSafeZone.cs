﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInSafeZone : MonoBehaviour
{

    [SerializeField] private Transform player = null;
    [SerializeField] private Vector2 playerZone = new Vector2(1.5f, 1.5f);
    [SerializeField] private Vector2 playerOffSet = Vector2.zero;

    private void FixedUpdate()
    {
        MoveToPlayerZone();
    }

    private void LateUpdate()
    {
        MoveToPlayerZone();
    }

    private void MoveToPlayerZone()
    {
        Vector3 cameraPos = this.transform.position;

        Vector2 playerPos = player.position;

        cameraPos.x = Mathf.Clamp(cameraPos.x, playerPos.x - playerOffSet.x - playerZone.x, playerPos.x - playerOffSet.x + playerZone.x);
        cameraPos.y = Mathf.Clamp(cameraPos.y, playerPos.y - playerOffSet.y - playerZone.y, playerPos.y - playerOffSet.y + playerZone.y);

        this.transform.position = cameraPos;
    }


}
