﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Threading.Tasks;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;
using UniRx.Async;
using UniRx.Async.Triggers;
using System.Threading;
using UniRx;
using UnityEngine.UI;


public class GamePlayController : MonoBehaviour
{



    public ControllerMenu controllerMenu = null;
    public MusicController musicController = null;


    [SerializeField] public float[] tempoMusicAtLevels = new float[] { 2, 3, 4, 5};
    [SerializeField] public float[] backgroundDeslizante = new float[] { 0.2f, 0.2f, 0.3f, 0.5f };

    [SerializeField] private Animation animations = null;
    [SerializeField] private Animation animations2 = null;
    //[SerializeField] private Animation animationEnemy = null;

    [SerializeField] private ParticleSystem particlePlayerAppear = null;
    [SerializeField] private ParticleSystem particleEnemyAppear = null;

    [SerializeField] private ParticleSystem[] buttonHitted = null;

    [SerializeField] private CanvasGroup fightCanvasGroup = null;


    //public static GamePlayController gameplayInstance;

    readonly string[] SPLIT_VALS = { "\n", "\r", "\r\n" };

    public bool started = false;


    [SerializeField] private TextAsset[] textPatterns;
    [SerializeField] private GameObject[] elementosPrefabs = null;
    [SerializeField] private Transform[] positionsSpawn;
    [SerializeField] private Transform parentSpawn;

    private List<List<char>> loadedData;
    private ushort contPositionSpawn = 0;
    private ushort yPositionSpawn = 0;
    private List<GameObject> lstGameobjects = new List<GameObject>();


    public ushort currentFase = 0;

    [SerializeField] private TextMeshProUGUI textanim = null;
    //[SerializeField] private ManagerBackgroundDeslizante managerBackgroundDeslizante = null;

    [SerializeField] private AudioSource[] audiosRayo = null;

    [SerializeField] private TextMeshProUGUI textPlayer = null;
    [SerializeField] private TextMeshProUGUI textEnemy = null;
    [SerializeField] private CanvasGroup canvasTextPlayer = null;
    [SerializeField] private CanvasGroup canvasTextEnemy = null;
    [SerializeField] private GameObject caraPlayer = null;
    [SerializeField] private GameObject caraEnemy = null;
    [SerializeField] private RectTransform caraPlayerTransform = null;
    [SerializeField] private RectTransform caraEnemyTransform = null;

    [SerializeField] private TextMeshProUGUI txtPuntuacionPlayer = null;
    [SerializeField] private TextMeshProUGUI txtPuntuacionEnemy = null;
    [HideInInspector] public uint puntuajePlayer = 0;
    [HideInInspector] public uint puntuajeEnemy = 0;

    [HideInInspector] public short health = 100;
    [SerializeField] private TextMeshProUGUI txtHealthPlayer = null;
    [SerializeField] private Image barHealthPlayer = null;

    private string[] charlaInicialLorca = new string[] {
        "Que pasa Chaval?",
        "Que pasa Crack?",
        "Illo que?",
        "Hola miarma!",
        "KE DISE ER TIO?",
        "Que pasa Artista?",
        "Mohtruoh!"

    };

    private string[] charlaInicialShakes = new string[] {
        "EYY BOY!!",
        "WAZAAAP BOY!! PSSSS",
        "You're MAN!",
        "EYYY! YOUUU!"

    };

    private string[] textosShakes = new string[] {

"Te seguiré, y de mi infierno haré un cielo si el que va a/ darme muerte es el que tanto/ yo quiero.",
"Esa engañosa palabra mañana, mañana, mañana, nos va/ llevando por días al sepulcro.",
"La falaz lumbre del ayer ilumina al necio hasta que cae/ en la fosa.",
"El pesar oculto, como un horno cerrado, quema el corazón/ hasta reducirlo en cenizas.",
"La honra no es más que una atribución vana y falsa que /suele ganarse sin mérito y perderse sin motivo.",
"El pobre contento es rico y bien rico; quien nada en /riquezas y teme perderlas es más/ pobre que el invierno.",
"La locura acierta a veces cuando el juicio y la/ cordura no dan fruto.",
"Hay más cosas en el cielo y en la tierra, que todas las/ que pueda soñar tu filosofía.",
"Podría estar encerrado en una cáscara de nuez y sentirme/ rey de un espacio infinito.",
"Ser o no ser, esa es la cuestión.",
"El abuso de la grandeza viene cuando se separa la/ clemencia del poder.",
"Lágrimas hay para su afecto, gozo para su fortuna, honra/ para su valor y muerte para su ambición.",
"El amor, como ciego que es, impide a los amantes ver las/ divertidas tonterías que cometen.",
"El gozo violento tiene un fin violento y muere en su/ éxtasis como fuego y pólvora, que, al unirse, estallan.",
"¡Ah, Romeo, Romeo! ¿Por qué eres Romeo? Niega a tu/ padre y rechaza tu nombre, o, si no,/ júrame tu amor y ya nunca seré una Capuleto.",
"Guarda más de lo que enseñas, di menos de lo que/ sepas.",
"Los seres perversos parecen hermosos al lado de otros/ más perversos: no ser lo peor/ también tiene mérito.",
"Al nacer, lloramos por haber venido a este gran teatro/ de locos.",
"Al nacer, lloramos por haber venido a este gran teatro/ de locos.",
"A mi entender, el amor y la callada sencillez si hablan/ menos, dicen más."


    };


    private string[] textosLorca = new string[] {


"El más terrible de todos los sentimientos es el /sentimiento de tener la esperanza muerta.",
"Vamos al rincón oscuro, donde yo siempre te quiera,/ que no me importa la gente, ni el veneno que nos echa.",
"Hay cosas encerradas dentro de los muros que, si/ salieran de pronto a la calle y gritaran, llenarían el mundo.",
"Es capaz de sentarse encima de tu corazón y ver /cómo te mueres durante un año sin que se le /cierre esa sonrisa fría que lleva en/ su maldita cara.",
"Verde que te quiero verde. Verde viento. Verdes ramas./ El barco sobre la mar y el caballo en la montaña.",
"Pero el dos no ha sido nunca un número porque es una/ angustia y su sombra.",
"He llegado a la línea donde cesa la nostalgia y la gota/ de llanto se transforma alabastro de espíritu.",
"Y aunque no me quisieras te querría por tu mirar sombrío/ como quiere la alondra al nuevo día, sólo por el rocío.",
"Nada turba los siglos pasados. No podemos arrancar un/ suspiro de lo viejo.",
"El primer beso que supo a beso y fue para mis labios/ niños como la lluvia fresca.",
"La soledad es la gran talladora del espíritu./ Impresiones y paisajes",
"En la bandera de la libertad bordé el amor más grande/ de mi vida.",
"Estas manos, que son tuyas, pero que al verte quisieran/ quebrar las ramas azules y el murmullo de tus venas.",
"¡Ay qué sinrazón! No quiero contigo cama ni cena, y no/ hay minuto del día que estar contigo no quiera,/ porque me arrastras y voy, y me dices que me vuelva y te sigo/ por el aire como una brizna de hierba.",
"La poesía no quiere adeptos, quiere amantes.",
"El que quiere arañar la luna, /se arañará el corazón.",
"Llena pues de palabras mi locura o déjame vivir en mi/ serena noche del alma, para siempre oscura.",
"En un arca de besos de bocas ya cerradas, es eterna/ cautiva, del corazón hermana.",
"No soy un hombre, ni un poeta, ni una hoja, sino un/ pulso herido que presiente el más allá.",
"Mira a la derecha y a la izquierda del tiempo y que/ tu corazón aprenda a estar tranquilo."




    };


    private List<List<string>> splitShakes = new List<List<string>>();
    private List<List<string>> splitLorca = new List<List<string>>();

    //public ushort contadorhash = 0;


    [SerializeField] private AudioSource aplausos = null;
    [SerializeField] private AudioSource bien = null;
    //[SerializeField] private AudioSource fallo = null;

    [SerializeField] private SpriteAtlas atlasLorca = null;
    [SerializeField] private SpriteAtlas atlasShakes = null;

    [SerializeField] private Animation animboard = null;
    [SerializeField] private SpriteRenderer[] botones = null;

    [SerializeField] private GameObject progresoPlayer = null;
    [SerializeField] private Color colorFase1 = Color.white;
    [SerializeField] private Color colorFase2 = Color.white;
    [SerializeField] private Color colorFase3 = Color.white;
    [SerializeField] private Color colorFase4 = Color.white;
    [SerializeField] private Sprite lorcaCaraSprite = null;
    [SerializeField] private Sprite shakeCaraSprite = null;
    private float progresoSuma = 0;
    [SerializeField] private Animation animProgresBar = null;

    [SerializeField] public ButtonController botonQ = null;
    [SerializeField] public ButtonController botonW = null;
    [SerializeField] public ButtonController botonE = null;
    [SerializeField] public ButtonController botonR = null;

    public ushort contadorAciertos = 0;
    public ushort contadorFallos = 0;

    public TextMeshProUGUI textfallos = null;
    public TextMeshProUGUI textaciertos = null;

    public Image playerImage = null;
    public Image enemigoImage = null;
    public SpriteRenderer board = null;

    public GameObject bocadilloPublicoLorca = null;
    public GameObject bocadilloPublicoShakes = null;
    private bool lorcaGolpeado = false;
    private bool shakesGolpeado = false;
    private ushort currentMusic = 2;
    [SerializeField] private TextMeshProUGUI textorepetir = null;
    private uint letrasTotal = 0;
    [SerializeField] private GameObject menuPause = null;
    [SerializeField] public ParticleSystem[] fuegoHold = null;

    [SerializeField] private GameObject fondoFight = null;
    [SerializeField] private RectTransform rectPlayer = null;
    [SerializeField] private RectTransform rectEnemy = null;

    [SerializeField] private RectTransform rectLorcaIzq = null;
    [SerializeField] private RectTransform rectShakesIzq = null;
    [SerializeField] private RectTransform rectLorcaDer = null;
    [SerializeField] private RectTransform rectShakesDer = null;

    [HideInInspector] public bool fromRetry = false;
    [SerializeField] private ClickedMenuButton[] clickedMenuButton = null;


    private void InstanciarLetras()
    {

        //print("instanciarletras");

        contPositionSpawn = 0;
        yPositionSpawn = 0;

        print("currentfase" + currentFase);
        
        loadedData = LoadDataText(textPatterns[currentFase]);
        loadedData.Reverse();

        for (ushort i = 0; i < loadedData.Count; i++)
        {
            var line = loadedData[i];

            for (ushort t = 0; t < line.Count; t++)
            {


                if (line[t] != '-')
                {

                    InstanciarBloques(line[t]);


                }


                if (contPositionSpawn >= 3)
                {
                    contPositionSpawn = 0;

                }
                else
                {

                    contPositionSpawn++;

                }

            }



            yPositionSpawn++;

        }


        



    }


    void InstanciarBloques(char patron)
    {
        //print(" instanciarBLOQUES");

        short index = -1;
        ButtonController buttonTemp = null;

        switch (patron)
        {

            case 'A': index = 0; buttonTemp = botonQ; break;
            case 'B': index = 1; buttonTemp = botonW; break;
            case 'C': index = 2; buttonTemp = botonE; break;
            case 'D': index = 3; buttonTemp = botonR; break;
            case 'S': index = 4; break;
            case '#': index = 5; break;
            //4 espacios
            case 'E': index = 6; buttonTemp = botonQ; break;
            case 'F': index = 7; buttonTemp = botonW; break;
            case 'G': index = 8; buttonTemp = botonE; break;
            case 'H': index = 9; buttonTemp = botonR; break;
            //6 espacios
            case 'I': index = 10; buttonTemp = botonQ; break;
            case 'J': index = 11; buttonTemp = botonW; break;
            case 'K': index = 12; buttonTemp = botonE; break;
            case 'M': index = 13; buttonTemp = botonR; break;

            //7 ESPACIOS
            case 'N': index = 14; buttonTemp = botonQ; break;
            case 'O': index = 15; buttonTemp = botonW; break;
            case 'P': index = 16; buttonTemp = botonE; break;
            case 'Q': index = 17; buttonTemp = botonR; break;


            //8 ESPACIOS
            case 'R': index = 18; buttonTemp = botonQ; break;
            case 'T': index = 19; buttonTemp = botonW; break;
            case 'U': index = 20; buttonTemp = botonE; break;
            case 'V': index = 21; buttonTemp = botonR; break;


            //9 ESPACIOS
            case 'W': index = 22; buttonTemp = botonQ; break;
            case 'X': index = 23; buttonTemp = botonW; break;
            case 'Y': index = 24; buttonTemp = botonE; break;
            case 'Z': index = 25; buttonTemp = botonR; break;




            case '*': index = 5; break;
            default:

                Debug.LogError("fallo formato 2=" + patron);
                
                return;
                
        }

        if (index == -1)
        {
            Debug.LogError("fallo formato 3=" + patron);
            return;

        }


        var go = GameObject.Instantiate
            (
                elementosPrefabs[index],

                new Vector3(
                    positionsSpawn[contPositionSpawn].position.x,
                    positionsSpawn[contPositionSpawn].position.y + yPositionSpawn,
                    positionsSpawn[contPositionSpawn].position.z
                    ),

                Quaternion.identity,
                parentSpawn
            );



        if (patron == 'E' || patron == 'F' || patron == 'G' || patron == 'H' ||
            patron == 'I' || patron == 'J' || patron == 'K' || patron == 'M' ||
            patron == 'N' || patron == 'O' || patron == 'P' || patron == 'Q' ||
            patron == 'R' || patron == 'T' || patron == 'U' || patron == 'V' ||
            patron == 'W' || patron == 'X' || patron == 'Y' || patron == 'Z'
            )
        {

            go.GetComponent<LetrasHoldCollider>().buttonController = buttonTemp;

            go.GetComponent<LetrasHoldCollider>().gamePlayController = this;
            go.GetComponent<LetrasHoldCollider>().letra = patron;


        }
        else
        {
            go.GetComponent<LetrasCollider>().buttonController = buttonTemp;
            go.GetComponent<LetrasCollider>().gamePlayController = this;
            go.GetComponent<LetrasCollider>().letra = patron;



        }


        go.GetComponent<LetrasScroller>().gamePlayController = this;
        go.GetComponent<LetrasScroller>().tempoMusica = tempoMusicAtLevels[currentFase];

        buttonTemp = null;


        lstGameobjects.Add(go);
    }


    private void Awake()
    {


        System.GC.Collect();

        bocadilloPublicoLorca.SetActive(false);
        progresoPlayer.SetActive(false);

        var anch = caraPlayerTransform.anchoredPosition;
        anch.x = 81.2f;
        caraPlayerTransform.anchoredPosition = anch;

        anch = caraEnemyTransform.anchoredPosition;
        anch.x = 81.2f;
        caraEnemyTransform.anchoredPosition = anch;


        menuPause.SetActive(false);

        splitShakes.Clear();
        splitLorca.Clear();

        for (ushort i = 0; i < textosShakes.Length; i++)
        {


            List<string> linea = textosShakes[i].Split('/').ToList();

            splitShakes.Add(
                linea
                );

          



        }

        for (ushort i = 0; i < textosLorca.Length; i++)
        {


            List<string> linea = textosLorca[i].Split('/').ToList();

            splitLorca.Add(
                linea
                );





        }

        fondoFight.SetActive(false);

        textPlayer.text = "";
        textEnemy.text = "";

        canvasTextEnemy.alpha = 0;
        canvasTextPlayer.alpha = 0;


        for (ushort i = 0; i < botones.Length; i++)
        {

            var b = botones[i].color;
            b.a = 0;
            botones[i].color = b;
        }



        fightCanvasGroup.alpha = 0;
       

     
        textanim.text = "A LEER!";

        InstanciarLetras();


    }


    private void Start()
    {
        fromRetry = false;
    }


    //List<UniRx.Async.UniTask> awaiters = new List<UniTask>();





    private CancellationTokenSource cancellationTokenSource1;
    private CancellationTokenSource cancellationTokenSource2 ;
    private CancellationTokenSource cancellationTokenSource3 ;
    private CancellationTokenSource cancellationTokenSource4 ;
    private CancellationTokenSource cancellationTokenSource5 ;
    private CancellationTokenSource cancellationTokenSource6 ;
    private CancellationTokenSource cancellationTokenSource7 ;
    private CancellationTokenSource cancellationTokenSource8 ;
    private CancellationTokenSource cancellationTokenSource9 ;
    private CancellationTokenSource cancellationTokenSource10;
    private CancellationTokenSource cancellationTokenSource11;
    private CancellationTokenSource cancellationTokenSource12;
    private CancellationTokenSource cancellationTokenSource13;



    public async void InitGame()
    {



        System.GC.Collect();


        audiosRayo[0].Stop();
        audiosRayo[1].Stop();

        particlePlayerAppear.Stop();
        particleEnemyAppear.Stop();

        fondoFight.SetActive(false);
        cancellationTokenSource1 = new CancellationTokenSource();
        cancellationTokenSource2 = new CancellationTokenSource();
        cancellationTokenSource3 = new CancellationTokenSource();
        cancellationTokenSource4 = new CancellationTokenSource();
        cancellationTokenSource5 = new CancellationTokenSource();
        cancellationTokenSource6 = new CancellationTokenSource();
        cancellationTokenSource7 = new CancellationTokenSource();
        cancellationTokenSource8 = new CancellationTokenSource();
        cancellationTokenSource9 = new CancellationTokenSource();
        cancellationTokenSource10 = new CancellationTokenSource();
        cancellationTokenSource11 = new CancellationTokenSource();
        cancellationTokenSource12 = new CancellationTokenSource();
        cancellationTokenSource13 = new CancellationTokenSource();

        print("initgame ");

        currentFase = 0;
        currentMusic = 2;
        contadorFallos = 0;
        health = 100;

        puntuajeEnemy = 0;
        puntuajePlayer = 0;

        txtPuntuacionEnemy.text = "Puntos=" + puntuajeEnemy.ToString("N0");
        txtPuntuacionPlayer.text = "Puntos=" + puntuajePlayer.ToString("N0");
        txtHealthPlayer.text = "Salud= " + health.ToString();
        barHealthPlayer.fillAmount = (health / 100f);



        PintarProgresoBar();


        if (fromRetry == false)
        {

            var p = playerImage.color;
            p.a = 0;
            playerImage.color = p;


            var e = enemigoImage.color;
            e.a = 0;
            enemigoImage.color = e;


            var b = board.color;
            b.a = 0;
            board.color = b;

            animations.Play("TransitionClose");
            await UniTask.Delay((int)animations.GetClip("TransitionClose").length * 1000, false, PlayerLoopTiming.Update, cancellationTokenSource1.Token);



        }






        if (cancellationTokenSource1.IsCancellationRequested)
        {

            print("cancelado");
            return;
        }


        controllerMenu.canvas[5].SetActive(true);
        controllerMenu.canvas[5].gameObject.SetActive(true);
        controllerMenu.canvas[3].SetActive(false);

        controllerMenu.paneles[0].SetActive(true);
        controllerMenu.paneles[1].SetActive(true);

        animations.Play("TransitionOpen");
        await UniTask.Delay(System.TimeSpan.FromMilliseconds( animations.GetClip("TransitionOpen").length * 1000), false, 
            PlayerLoopTiming.Update, cancellationTokenSource2.Token);

        if (cancellationTokenSource2.IsCancellationRequested)
        {

            print("cancelado");
            return;
        }

        if (fromRetry == false)
        {



            animations2.Play("OpenCortinas");
        }



        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");
                enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");


                rectPlayer.anchoredPosition = rectLorcaIzq.anchoredPosition;
                rectPlayer.sizeDelta = rectLorcaIzq.sizeDelta;

                rectEnemy.anchoredPosition = rectShakesDer.anchoredPosition;
                rectEnemy.sizeDelta = rectShakesDer.sizeDelta;


                break;
            case "shakes":

                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");
                enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                rectPlayer.anchoredPosition = rectShakesIzq.anchoredPosition;
                rectPlayer.sizeDelta = rectShakesIzq.sizeDelta;

                rectEnemy.anchoredPosition = rectLorcaDer.anchoredPosition;
                rectEnemy.sizeDelta = rectLorcaDer.sizeDelta;


                break;
            default:
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");
                enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                rectPlayer.anchoredPosition = rectShakesIzq.anchoredPosition;
                rectPlayer.sizeDelta = rectShakesIzq.sizeDelta;

                rectEnemy.anchoredPosition = rectLorcaDer.anchoredPosition;
                rectEnemy.sizeDelta = rectLorcaDer.sizeDelta;


                break;
        }






        //ushort vol = (ushort)PlayerPrefs.GetInt("mainSoundInternal");
        musicController.SetVolumenSounds(11);

        musicController.PlayInGameMusic(
            musicController.sounds[currentMusic], true

        );



        if (fromRetry == false)
        {

            await UniTask.Delay(1500, false, PlayerLoopTiming.Update, cancellationTokenSource3.Token);

            if (cancellationTokenSource3.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }

            audiosRayo[0].Play();
            audiosRayo[1].Play();



            particlePlayerAppear.Play();
            particleEnemyAppear.Play();
            progresoPlayer.SetActive(true);
            animations.Play("AparecerPlayer");

            int t = (int)((animations.GetClip("AparecerPlayer").length * 1000));
            await UniTask.Delay(t, false, PlayerLoopTiming.Update, cancellationTokenSource4.Token);

            if (cancellationTokenSource4.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }

            animations.Play("AparecerEnemy");
            await UniTask.Delay(200, false, PlayerLoopTiming.Update, cancellationTokenSource5.Token);

            if (cancellationTokenSource5.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }


            //PintarProgresoBar();
            InitProgresoBar();

            aplausos.Play();

            await UniTask.Delay(800, false, PlayerLoopTiming.Update, cancellationTokenSource6.Token);

            if (cancellationTokenSource6.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }


            CharlaInicial();
            await UniTask.Delay(3000, false, PlayerLoopTiming.Update, cancellationTokenSource7.Token);
            if (cancellationTokenSource7.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }


        }
        else
        {


            var anch = caraPlayerTransform.anchoredPosition;
            anch.x = -94;
            caraPlayerTransform.anchoredPosition = anch;

            var tanch = caraEnemyTransform.anchoredPosition;
            tanch.x = -94;
            caraEnemyTransform.anchoredPosition = tanch;

            progresoPlayer.SetActive(true);

        }


        textfallos.text = "Fallos: " + contadorFallos + " H:" + health;
        
        animboard.Play("TransitionBoard");

        fondoFight.SetActive(true);
        animations.Play("Fight");
        int s = (int)((animations.GetClip("Fight").length * 1000) + 500);
        await UniTask.Delay(s, false, PlayerLoopTiming.Update, cancellationTokenSource8.Token);
        if (cancellationTokenSource8.IsCancellationRequested)
        {

            print("cancelado");
            return;
        }



        audiosRayo[0].Stop();
        audiosRayo[1].Stop();

        ushort vol = (ushort)PlayerPrefs.GetInt("mainSoundInternal");
        musicController.SetVolumenSounds(vol);
        started = true;

        aplausos.Stop();
        musicController.StopSFXSound();

        musicController.PlayInGameMusic( musicController.sounds[currentMusic], true );


    }

   

    private async void CharlaInicial()
    {

        //print("lorca len=" + charlaInicialLorca.Length);

        if (controllerMenu.playerName == "lorca")
        {

            var rnd = UnityEngine.Random.Range(0, charlaInicialLorca.Length);
            textPlayer.text =  charlaInicialLorca[rnd];
            rnd = UnityEngine.Random.Range(0, charlaInicialShakes.Length);
            textEnemy.text = charlaInicialShakes[rnd];


        }
        else
        {

            var rnd = UnityEngine.Random.Range(0, charlaInicialShakes.Length);
            textPlayer.text = charlaInicialShakes[rnd];
            rnd = UnityEngine.Random.Range(0, charlaInicialLorca.Length);
            textEnemy.text = charlaInicialLorca[rnd];

        }


        animations.Play("AparecerBocadilloPlayer");
        await UniTask.Delay(1000);
        if (animations == null) return;
        animations.Play("AparecerBocadilloEnemy");

        await UniTask.Delay(1000);

        animations.Play("DesaparacerBocadilloPlayer");
        await UniTask.Delay((int)(animations.GetClip("DesaparacerBocadilloPlayer").length * 1000));
        
        animations.Play("DesaparecerBocadilloEnemy");
        //await UniTask.Delay((int)(animations.GetClip("DesaparecerBocadilloEnemy").length * 1000));

    }

    public void ShowFrases()
    {


        if (controllerMenu.playerName == "lorca")
        {


            //print("lorca");
            ShowFraseLorca();
        }
        else
        {
            //print("shakes");
            ShowFraseShakes();
        }



    }

    public async void ShowFraseShakes()
    {

        //Te seguiré, y de mi infierno haré un cielo si el que va a/ darme muerte es el que tanto yo quiero. Sueño de una noche de verano
        var rnd = UnityEngine.Random.Range(0, splitShakes.Count);

        //for (ushort o = 0; o < splitShakes.Count; o++)
        {

            List<string> linea = splitShakes[rnd];
            if (animations == null) return;

            animations.Play("AparecerBocadilloPlayer");
            //animations.Play("AparecerBocadilloEnemy");

            for (ushort i = 0; i < linea.Count; i++)
            {

                textPlayer.text = linea[i];
                await UniTask.Delay(2500, false, PlayerLoopTiming.Update, cancellationTokenSource9.Token);
            }


        }


        animations.Play("DesaparacerBocadilloPlayer");







    }



    public async void ShowFraseLorca()
    {

        var rnd = UnityEngine.Random.Range(0, splitLorca.Count);

        //for (ushort o = 0; o < splitLorca.Count; o++)
        {

            List<string> linea = splitLorca[rnd];
            animations.Play("AparecerBocadilloPlayer");

            for (ushort i = 0; i < linea.Count; i++)
            {

                textPlayer.text = linea[i];

                await UniTask.Delay(2500, false, PlayerLoopTiming.Update, cancellationTokenSource9.Token);
                

            }


        }



        //await UniTask.Delay(200);

        animations.Play("DesaparacerBocadilloPlayer");
        //await UniTask.Delay((int)(animation.GetClip("DesaparacerBocadilloPlayer").length * 1000));







    }



    public async void EnemyPrincipioHold(uint puntuaje)
    {
        //print("* enemyFIRST?");

        int rnd = UnityEngine.Random.Range(0, 2);
        if (rnd == 1) //enemigo acertado
        {

            //print("enemy PRINCIPIO ACERTADO");
            puntuajeEnemy += puntuaje;
            txtPuntuacionEnemy.text = "Puntos=" + puntuajeEnemy.ToString("N0");

            var anch = caraEnemyTransform.anchoredPosition;
            anch.x += progresoSuma / 2;
            caraEnemyTransform.anchoredPosition = anch;


            switch (controllerMenu.playerName)
            {

                case "lorca":

                    enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                    break;
                case "shakes":
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                    break;
                default:
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                    break;
            }

            await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource13.Token);
            if (cancellationTokenSource13.IsCancellationRequested == true)
            {

                print("13 cancelado");
                return;

            }

            switch (controllerMenu.playerName)
            {

                case "lorca":
                    enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");


                    break;
                case "shakes":
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
                default:
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
            }


        }
    }

    public async void EnemyCompleteHold(uint puntuaje)
    {

        //print("* enemycomplete?");
        int rnd = UnityEngine.Random.Range(0, 2);
        if (rnd == 1) //enemigo acertado
        {

            //print("enemy COMPLETE ACERTADO");
            puntuajeEnemy += puntuaje;
            txtPuntuacionEnemy.text = "Puntos=" + puntuajeEnemy.ToString("N0");

            var anch = caraEnemyTransform.anchoredPosition;
            anch.x += progresoSuma / 2;
            caraEnemyTransform.anchoredPosition = anch;

            switch (controllerMenu.playerName)
            {

                case "lorca":

                    enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                    break;
                case "shakes":
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                    break;
                default:
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                    break;
            }

            await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource13.Token);
            if (cancellationTokenSource13.IsCancellationRequested == true)
            {

                print("13 cancelado");
                return;

            }

            switch (controllerMenu.playerName)
            {

                case "lorca":

                    enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                    break;
                case "shakes":
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
                default:
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
            }


        }
    }

    public async void PlayerPrincipioHold(uint puntuaje)
    {


        puntuajePlayer += puntuaje;
        txtPuntuacionPlayer.text = "Puntos=" + puntuajePlayer.ToString("N0");

        var anch = caraPlayerTransform.anchoredPosition;
        anch.x += progresoSuma / 2;
        caraPlayerTransform.anchoredPosition = anch;


        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");


                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                break;
            default:
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                break;
        }

        await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource13.Token);
        if (cancellationTokenSource13.IsCancellationRequested == true)
        {

            print("13 cancelado");
            return;

        }

        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");


                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                break;
            default:
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                break;
        }

    }

    public async void PlayerCompleteHold(ushort index, uint puntuaje)
    {

        buttonHitted[index].Play();

        musicController.StopSFXSound();

        musicController.PlayFX2Sound(
            musicController.sfx[7]
            );



        puntuajePlayer += puntuaje;
        txtPuntuacionPlayer.text = "Puntos=" + puntuajePlayer.ToString("N0");


        contadorAciertos++;
        textaciertos.text = "Aciertos: " + contadorAciertos.ToString();

        var anch = caraPlayerTransform.anchoredPosition;
        anch.x += progresoSuma / 2;
        caraPlayerTransform.anchoredPosition = anch;


        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");


                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                break;
            default:
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                break;
        }


        await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource13.Token);
        if (cancellationTokenSource13.IsCancellationRequested == true)
        {

            print("13 cancelado");
            return;

        }

        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");


                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                break;
            default:
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                break;
        }




    }

    public void PressedButton(ushort index)
    {

        //buttonHitted[index].Play();

        musicController.StopSFXSound();

        musicController.PlayFXSound(
            musicController.sfx[12]
            );

    }

    public async void ShowEnemyPuntaje(uint puntuaje)
    {

        int rnd = UnityEngine.Random.Range(0, 2);
        if (rnd == 1) //enemigo acertado
        {


            puntuajeEnemy += puntuaje;
            txtPuntuacionEnemy.text = "Puntos=" + puntuajeEnemy.ToString("N0");


            var anch = caraEnemyTransform.anchoredPosition;
            anch.x += progresoSuma;
            caraEnemyTransform.anchoredPosition = anch;

            switch (controllerMenu.playerName)
            {

                case "lorca":

                    enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                    break;
                case "shakes":
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                    break;
                default:
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                    break;
            }

            await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource13.Token);
            if (cancellationTokenSource13.IsCancellationRequested == true)
            {

                print("13 cancelado");
                return;

            }

            switch (controllerMenu.playerName)
            {

                case "lorca":
                    enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");


                    break;
                case "shakes":
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
                default:
                    enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
            }



        }



    }

   

    public void AciertoBoton(ushort index, char letra, uint puntuaje)
    {


        buttonHitted[index].Play();

        musicController.StopSFXSound();

        musicController.PlayFX2Sound(
            musicController.sfx[7]
            );


        puntuajePlayer += puntuaje;
        txtPuntuacionPlayer.text = "Puntos=" + puntuajePlayer.ToString("N0");


        contadorAciertos++;
        textaciertos.text = "Aciertos: " +  contadorAciertos.ToString();

        var anch = caraPlayerTransform.anchoredPosition;
        anch.x += progresoSuma;
        caraPlayerTransform.anchoredPosition = anch;





    }


    public async void DesAciertoBoton()
    {
        print("kaka");

        health -= 5;
        contadorFallos++;

        if (health <= 0)
        {
            health = 0;
            textfallos.text = "Fallos: " + contadorFallos + " H:" + health;
            txtHealthPlayer.text = "Salud= " + health.ToString();
            barHealthPlayer.fillAmount = 0;
            ShowPerdedor();

            return;

        }

        textfallos.text = "Fallos: " + contadorFallos + " H:" + health;
        txtHealthPlayer.text = "Salud= " + health.ToString();
        barHealthPlayer.fillAmount = (health / 100f);


        if (started == true)
        {


            //ShowEnemyPuntaje();
            switch (controllerMenu.playerName)
            {

                case "lorca":
                    playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose2");


                    break;
                case "shakes":
                    playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose2");

                    break;
                default:
                    playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose2");

                    break;
            }


            musicController.StopSFXSound();
            musicController.PlayFXSound(
               musicController.sfx[13]
               );

            await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource13.Token);
            if (cancellationTokenSource13.IsCancellationRequested == true)
            {

                print("13 cancelado");
                return;

            }
            if (started == false) return;


            switch (controllerMenu.playerName)
            {

                case "lorca":
                    playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                    break;
                case "shakes":
                    playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                    break;
                default:
                    playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                    break;
            }





        }
    }


    public void DejadoPasar()
    {

        health -= 5;
        contadorFallos++;
        print("dejado pasar");

        if (health <= 0)
        {
            health = 0;
            textfallos.text = "Fallos: " + contadorFallos + " H:" + health;
            txtHealthPlayer.text = "Salud= " + health.ToString();
            barHealthPlayer.fillAmount = 0;
            ShowPerdedor();
            return;


        }
        textfallos.text = "Fallos= " + contadorFallos + " H=" + health;
        txtHealthPlayer.text = "Salud= " + health.ToString();
        barHealthPlayer.fillAmount = (health / 100f);



        switch (controllerMenu.playerName)
        {

            case "lorca": FalloLorca();  break;
            case "shakes": FalloShakes();  break;

        }




    }


    private async void FalloLorca()
    {


   


        if (lorcaGolpeado == false)
        {

            lorcaGolpeado = true;
            playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose2");

            bocadilloPublicoLorca.SetActive(true);
            musicController.PlayFX2Sound(
         musicController.sfx[13]
         );



            await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource10.Token);
            if (cancellationTokenSource10.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }


            if (started == false) return;

            lorcaGolpeado = false;
            playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");
            //enemigoImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");
            bocadilloPublicoLorca.SetActive(false);



        }
    }


    private async void FalloShakes()
    {

        if (shakesGolpeado == false)
        {

            playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose2");
            //enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

            bocadilloPublicoLorca.SetActive(true);
            musicController.PlayFX2Sound(  musicController.sfx[13] );

            await UniTask.Delay(1000, false, PlayerLoopTiming.Update, cancellationTokenSource11.Token);

            if (cancellationTokenSource11.IsCancellationRequested)
            {

                print("cancelado");
                return;
            }

            if (started == false) return;

            shakesGolpeado = false;
            playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");
            //enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");
            bocadilloPublicoLorca.SetActive(false);
        }
    }


    public async void NextFase()
    {
        print("currenfase=" + currentFase + " currentmusic=" + currentMusic);

        if (playerImage == null) return;

        currentFase++;
        currentMusic++;
        switch (currentFase)
        {

            case 0:


                textanim.text = string.Format("A LEER!"); break;
            case 1:

                textanim.text = string.Format("COMA!"); break;
            case 2:
                textanim.text = string.Format("PUNTO Y COMA!"); break;
            case 3:


                textanim.text = string.Format("PUNTO FINAL!"); break;
            default:

                print("final?");
                ShowPremio();
                return;

        }


        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                break;
            default:
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                break;
        }


        musicController.StopSFXSound();
        contadorAciertos = 0;

        bien.Play();

        for (int i = 0; i < lstGameobjects.Count; i++)
        {
            Destroy(lstGameobjects[i]);

        }

        lstGameobjects.Clear();

        var gos = GameObject.FindGameObjectsWithTag("letras");
        for (int i = 0; i < gos.Length; i++)
        {
            Destroy(gos[i]);

        }



        await UniTask.Delay(500);
        fondoFight.SetActive(true);
        animations.Play("Fight");
        int t = (int)((animations.GetClip("Fight").length * 1000) + 500);

        await UniTask.Delay(t);

     

        print("currentfase=" + currentFase  + " currentmusic=" + currentMusic);
        
        musicController.PlayInGameMusic(
            musicController.sounds[currentMusic], true
        );

        InstanciarLetras();

        PintarProgresoBar();
        InitProgresoBar();


        await UniTask.Delay(1000);


        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose1");

                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                break;
            default:
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose1");

                break;
        }




    }


    private void ShowPerdedor()
    {

        started = false;
        fondoFight.SetActive(true);
        
        bocadilloPublicoLorca.SetActive(true);

        switch (controllerMenu.playerName)
        {

            case "lorca":
                playerImage.sprite = atlasLorca.GetSprite("Lorca_Pose2");
                enemigoImage .sprite = atlasShakes.GetSprite("Shakespeare_pose3");

                break;
            case "shakes":
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose2");
                enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                break;
            default:
                playerImage.sprite = atlasShakes.GetSprite("Shakespeare_pose2");
                enemigoImage.sprite = atlasLorca.GetSprite("Lorca_Pose3");

                break;
        }

        textorepetir.text = "RETRY";

        musicController.PlayInGameMusic(  musicController.sounds[10], false  );
        musicController.PlayFXSound(    musicController.sfx[11]  );

        

        textanim.text = string.Format("YOU LOSE!");

        animations.Play("Perder");
    


    }

    private void ShowPremio()
    {



        fondoFight.SetActive(true);
        textorepetir.text = "PLAY AGAIN";

        musicController.PlayInGameMusic(
          musicController.sounds[9], false
      );

        musicController.PlayFXSound(
          musicController.sfx[5]
      );

        started = false;

        textanim.text = string.Format("YOU WIN!");


      
        animations.Play("Ganar");
        //int t = (int)((animations.GetClip("Fight").length * 1000) + 500);


        //await UniTask.Delay(t);

        //print("ganador");
    }


    private List<List<char>> LoadDataText(TextAsset blockTextData)
    {

        letrasTotal = 0; 
        var data = new List<List<char>>();


        string[] lines = blockTextData.text.Split(SPLIT_VALS, System.StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < lines.Length; i++)
        {
            data.Add(new List<char>(4));

            for (int j = 0; j < 4; j++)
            {

                switch (lines[i][j])
                {

                    case 'A': data[data.Count - 1].Add('A'); letrasTotal++;  break;
                    case 'B': data[data.Count - 1].Add('B'); letrasTotal++; break;
                    case 'C': data[data.Count - 1].Add('C'); letrasTotal++; break;
                    case 'D': data[data.Count - 1].Add('D'); letrasTotal++; break;
                        //4 espacios
                    case 'E': data[data.Count - 1].Add('E'); letrasTotal++; break;
                    case 'F': data[data.Count - 1].Add('F'); letrasTotal++; break;
                    case 'G': data[data.Count - 1].Add('G'); letrasTotal++; break;
                    case 'H': data[data.Count - 1].Add('H'); letrasTotal++; break;

                    //6 espacios
                    case 'I': data[data.Count - 1].Add('I'); letrasTotal++; break;
                    case 'J': data[data.Count - 1].Add('J'); letrasTotal++; break;
                    case 'K': data[data.Count - 1].Add('K'); letrasTotal++; break;
                    case 'M': data[data.Count - 1].Add('M'); letrasTotal++; break;


                    //7 espacios
                    case 'N': data[data.Count - 1].Add('N'); letrasTotal++; break;
                    case 'O': data[data.Count - 1].Add('O'); letrasTotal++; break;
                    case 'P': data[data.Count - 1].Add('P'); letrasTotal++; break;
                    case 'Q': data[data.Count - 1].Add('Q'); letrasTotal++; break;
                    //8 espacios
                    case 'R': data[data.Count - 1].Add('R'); letrasTotal++; break;
                    case 'T': data[data.Count - 1].Add('S'); letrasTotal++; break;
                    case 'U': data[data.Count - 1].Add('T'); letrasTotal++; break;
                    case 'V': data[data.Count - 1].Add('U'); letrasTotal++; break;




                    //9 espacios
                    case 'W': data[data.Count - 1].Add('W'); letrasTotal++; break;
                    case 'X': data[data.Count - 1].Add('X'); letrasTotal++; break;
                    case 'Y': data[data.Count - 1].Add('Y'); letrasTotal++; break;
                    case 'Z': data[data.Count - 1].Add('Z'); letrasTotal++; break;




                    case '#': data[data.Count - 1].Add('#'); break;
                    case 'S': data[data.Count - 1].Add('S'); break;

                    case '-': data[data.Count - 1].Add('-'); break;
                    case '*': data[data.Count - 1].Add('*'); break;
                    default:
                        Debug.LogError("fallo formato 2=" + lines[i][j]);
                        
                        break;
                }

            }
        }

        print("letrastotal="+ letrasTotal);
        return data;


    }



    private void OnDisable()
    {

        for (int i = 0; i < lstGameobjects.Count; i++)
        {

            Destroy(lstGameobjects[i]);

        }



        if (cancellationTokenSource1.Token.CanBeCanceled == true) cancellationTokenSource1.Cancel(false);
        if (cancellationTokenSource2.Token.CanBeCanceled == true) cancellationTokenSource2.Cancel(false);
        if (cancellationTokenSource3.Token.CanBeCanceled == true) cancellationTokenSource3.Cancel(false);
        if (cancellationTokenSource4.Token.CanBeCanceled == true) cancellationTokenSource4.Cancel(false);
        if (cancellationTokenSource5.Token.CanBeCanceled == true) cancellationTokenSource5.Cancel(false);
        if (cancellationTokenSource6.Token.CanBeCanceled == true) cancellationTokenSource6.Cancel(false);
        if (cancellationTokenSource7.Token.CanBeCanceled == true) cancellationTokenSource7.Cancel(false);
        if (cancellationTokenSource8.Token.CanBeCanceled == true) cancellationTokenSource8.Cancel(false);
        if (cancellationTokenSource9.Token.CanBeCanceled == true) cancellationTokenSource9.Cancel(false);
        if (cancellationTokenSource10.Token.CanBeCanceled == true) cancellationTokenSource10.Cancel(false);


        if (cancellationTokenSource11 != null)
        {

            if (cancellationTokenSource11.Token.CanBeCanceled == true) cancellationTokenSource11.Cancel(false);
            cancellationTokenSource11.Dispose();

        }

        if (cancellationTokenSource12.Token.CanBeCanceled == true) cancellationTokenSource12.Cancel(false);
        if (cancellationTokenSource13.Token.CanBeCanceled == true) cancellationTokenSource13.Cancel(false);

        cancellationTokenSource1.Dispose();
        cancellationTokenSource2.Dispose();
        cancellationTokenSource3.Dispose();
        cancellationTokenSource4.Dispose();
        cancellationTokenSource5.Dispose();
        cancellationTokenSource6.Dispose();
        cancellationTokenSource7.Dispose();
        cancellationTokenSource8.Dispose();
        cancellationTokenSource9.Dispose();
        cancellationTokenSource10.Dispose();

        cancellationTokenSource12.Dispose();
        cancellationTokenSource13.Dispose();


        //Destroy(this.gameObject);

        //DestroyImmediate(musicController);
        System.GC.Collect();

    }



    public void Reintentar()
    {

       
        fondoFight.SetActive(false);
        if (controllerMenu.canvas[5].activeSelf == false) return;

        print("reintentar");
        Time.timeScale = 1;
        clickedMenuButton[0].isClickedMenu = false;
        clickedMenuButton[1].isClickedMenu = false;

        for (int i = 0; i < lstGameobjects.Count; i++)
        {
            Destroy(lstGameobjects[i]);

        }

        lstGameobjects.Clear();

        var gos = GameObject.FindGameObjectsWithTag("letras");
        for (int i = 0; i < gos.Length; i++)
        {
            Destroy(gos[i]);

        }


        if (cancellationTokenSource1.Token.CanBeCanceled == true) cancellationTokenSource1.Cancel(false);
        if (cancellationTokenSource2.Token.CanBeCanceled == true) cancellationTokenSource2.Cancel(false);
        if (cancellationTokenSource3.Token.CanBeCanceled == true) cancellationTokenSource3.Cancel(false);
        if (cancellationTokenSource4.Token.CanBeCanceled == true) cancellationTokenSource4.Cancel(false);
        if (cancellationTokenSource5.Token.CanBeCanceled == true) cancellationTokenSource5.Cancel(false);
        if (cancellationTokenSource6.Token.CanBeCanceled == true) cancellationTokenSource6.Cancel(false);
        if (cancellationTokenSource7.Token.CanBeCanceled == true) cancellationTokenSource7.Cancel(false);
        if (cancellationTokenSource8.Token.CanBeCanceled == true) cancellationTokenSource8.Cancel(false);
        if (cancellationTokenSource9.Token.CanBeCanceled == true) cancellationTokenSource9.Cancel(false);
        if (cancellationTokenSource10.Token.CanBeCanceled == true) cancellationTokenSource10.Cancel(false);


        if (cancellationTokenSource11 != null)
        {

            if (cancellationTokenSource11.Token.CanBeCanceled == true) cancellationTokenSource11.Cancel(false);
            cancellationTokenSource11.Dispose();

        }

        if (cancellationTokenSource12.Token.CanBeCanceled == true) cancellationTokenSource12.Cancel(false);
        if (cancellationTokenSource13.Token.CanBeCanceled == true) cancellationTokenSource13.Cancel(false);

        cancellationTokenSource1.Dispose();
        cancellationTokenSource2.Dispose();
        cancellationTokenSource3.Dispose();
        cancellationTokenSource4.Dispose();
        cancellationTokenSource5.Dispose();
        cancellationTokenSource6.Dispose();
        cancellationTokenSource7.Dispose();
        cancellationTokenSource8.Dispose();
        cancellationTokenSource9.Dispose();
        cancellationTokenSource10.Dispose();

        cancellationTokenSource12.Dispose();
        cancellationTokenSource13.Dispose();

        canvasTextEnemy.alpha = 0;
        canvasTextPlayer.alpha = 0;

        


        started = false;

        System.GC.Collect();

        currentFase = 0;
        currentMusic = 2;

        controllerMenu.canvas[5].SetActive(false);
        controllerMenu.DesactivarCanvas();

        var b = board.color;
        b.a = 0;
        board.color = b;

        fromRetry = true;
        controllerMenu.isPlayerSetted = false;


        Awake();
        controllerMenu.GoToSelectPersonaje();

        //InitGame();


        
    }


    public void Salir()
    {

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

#else


        Application.Quit();
#endif
    }


    public void ShowMenuPause()
    {
        Time.timeScale = 0;
        menuPause.SetActive(true);

    }


    public void DisableShowMenu()
    {
        Time.timeScale = 1;
        menuPause.SetActive(false);

    }

    private void Update()
    {
        if (controllerMenu.canvas[5].activeSelf == false) return;

        if (Input.GetKeyDown(KeyCode.Escape) == true)
        {


            ShowMenuPause();

        }

    }




    private void PintarProgresoBar()
    {

        if (controllerMenu.playerName == "lorca")
        {
            caraPlayer.GetComponent<Image>().sprite = lorcaCaraSprite;
            caraEnemy.GetComponent<Image>().sprite = shakeCaraSprite;

        }
        else
        {

            caraPlayer.GetComponent<Image>().sprite = shakeCaraSprite;
            caraEnemy.GetComponent<Image>().sprite = lorcaCaraSprite;
        }


        switch (currentFase)
        {
            case 0: progresoPlayer.GetComponent<Image>().color = colorFase1; break;
            case 1: progresoPlayer.GetComponent<Image>().color = colorFase2; break;
            case 2: progresoPlayer.GetComponent<Image>().color = colorFase3; break;
            case 3: progresoPlayer.GetComponent<Image>().color = colorFase4; break;
            default:
                Debug.LogError("color kakka");
                return;

        }

    }

  
    private async void InitProgresoBar()
    {

        progresoSuma = 181f / letrasTotal;

        animProgresBar.Play("InitProgresoBar");

        await UniTask.Delay(System.TimeSpan.FromMilliseconds(animProgresBar.GetClip("InitProgresoBar").length * 1000), false, PlayerLoopTiming.Update, 
            cancellationTokenSource12.Token);


        if (cancellationTokenSource12.IsCancellationRequested)
        {

            print("cancelado");
            return;
        }

        var anch = caraPlayerTransform.anchoredPosition;
        anch.x = -94;
        caraPlayerTransform.anchoredPosition = anch;

        var tanch = caraEnemyTransform.anchoredPosition;
        tanch.x = -94;
        caraEnemyTransform.anchoredPosition = tanch;



    }

}
