﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollowTarget : MonoBehaviour
{

    
    [SerializeField] private Transform player = null;
    [SerializeField] private float smoothTime = 0.5f;
    [SerializeField] private Vector3 camVelocity = Vector3.zero;

    private void FixedUpdate()
    {

        Vector3 camSmoothPos = Vector3.SmoothDamp(this.transform.position, player.position, ref camVelocity, smoothTime);
        camSmoothPos.z = this.transform.position.z;

        this.transform.position = camSmoothPos;
    }


}
