﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetrasCollider : MonoBehaviour
{

    [SerializeField] private bool canButtonBePressed = false;

    [SerializeField] private KeyCode keyNeededTobePressed = KeyCode.None;
    //[SerializeField] private KeyCode otrokeyNeededTobePressed = KeyCode.None;

    [SerializeField] public char letra = '0';
    [SerializeField] public GamePlayController gamePlayController = null;
    [SerializeField] public bool isKeyNeedHold = false;
    [SerializeField] public bool isTrueHold = false;
    [SerializeField] public bool isHoldCompleted = false;
    [SerializeField] public ButtonController buttonController = null;


    // Start is called before the first frame update
    void Start()
    {

    }



    private void Update()
    {



        if (gamePlayController == null || gamePlayController.started == false) return;

        if (Input.GetKeyDown(keyNeededTobePressed) == true)
        {

            if (canButtonBePressed == true)
            {

                StartCoroutine(buttonController.SetColorToPress());

                switch (keyNeededTobePressed)
                {

                    case KeyCode.Q: gamePlayController.AciertoBoton(0, letra, 100); break;
                    case KeyCode.W: gamePlayController.AciertoBoton(1, letra, 100); break;
                    case KeyCode.E: gamePlayController.AciertoBoton(2, letra, 100); break;
                    case KeyCode.R: gamePlayController.AciertoBoton(3, letra, 100); break;

                }

                canButtonBePressed = false;
                ComprobarNextLevel();
                Destroy(this.gameObject);




            }


        }





    }


    private void ComprobarNextLevel()
    {

        if (letra == 'S')
        {

            print("next level");
            gamePlayController.NextFase();

        }
    }


    private void OnTriggerEnter2D(Collider2D other)
    {

        //botones
        if (other.tag == "Botones")
        {

                      

            canButtonBePressed = true;
        }
       
        //zona destroy
        if (other.tag == "Destroy")
        {

           


                if (letra != 'S' && letra != '#' && letra != '*')
                {

                    gamePlayController.DejadoPasar();
                    gamePlayController.ShowEnemyPuntaje(100);
                

                }

                if (letra == '#')
                {

                    gamePlayController.ShowFrases();


                }

                ComprobarNextLevel();
                Destroy(this.gameObject);



            
            



        }


    }

  

    private void OnTriggerExit2D(Collider2D other)
    {


        //if (letra == 'S' && letra == '#') return;


        if (other.tag == "Botones")
        {
            canButtonBePressed = false;

           
            


        }

    }

}
