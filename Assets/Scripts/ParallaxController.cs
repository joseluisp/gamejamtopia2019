﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxController : MonoBehaviour
{
        
    

    public enum Modo
    {
        Uniform,
        ByAxis,
    }

    //public enum EMasterCameraSelectionMode
    //{
    //    MainCamera,
    //    ByReference,
    //    ByName,
    //    ByTag,
    //}

    [System.Serializable]
    public class FreezeAxes
    {
        public bool x;
        public bool y;
    }

    [System.Serializable]
    public class ParallaxByAxis
    {
        public float x = 0.5f;
        public float y = 0.5f;
    }

    [SerializeField] private Modo mode;

    // Uniform mode
    [SerializeField] private float parallax = 0.5f;
    [SerializeField] private FreezeAxes freezeAxes = null;

    // By Axis mode
    [SerializeField] private ParallaxByAxis parallaxByAxis;

    [SerializeField] private Camera cam = null;

    private Vector3 camLastPosition = Vector3.zero;

    private void Start()
    {
        camLastPosition = cam.transform.position;
    }

    private void LateUpdate()
    {
        UpdateCamMovement();
    }

    private void UpdateCamMovement()
    {
        if (cam is null) return;
        
        Vector3 cameraCurrentPos = cam.transform.position;

        Vector3 smoothCamMovement = cameraCurrentPos - camLastPosition;

        smoothCamMovement.z = 0;
        switch (mode)
        {
            case Modo.Uniform:
            
                smoothCamMovement *= parallax;

                if (freezeAxes.x)
                {
                    smoothCamMovement.x = 0;
                }

                if (freezeAxes.y)
                {
                    smoothCamMovement.y = 0;
                }
            
            break;

            case Modo.ByAxis:
           
                smoothCamMovement.x *= parallaxByAxis.x;
                smoothCamMovement.y *= parallaxByAxis.y;
            
                break;
        }

        this.transform.position += smoothCamMovement;

        camLastPosition = cameraCurrentPos;
    }

   

 



}
