﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class ClickedMenuButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private RectTransform viewport = null;

    [SerializeField] private ParticleSystem clickParticle = null;
    [SerializeField] private RectTransform particleRec = null;
    //[SerializeField] private ControllerMenu controllerMenu = null;

    [HideInInspector] public bool isClickedMenu = false;

    public void OnPointerClick(PointerEventData eventData)
    {



        if (clickParticle.isPlaying == false && isClickedMenu == false)
        {

            isClickedMenu = true;

            Vector2 localPos = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(viewport, eventData.position, eventData.pressEventCamera, out localPos) == false)
            {
                return;
            }

            particleRec.anchoredPosition = localPos;
            clickParticle.Play();




        }

    }

}
