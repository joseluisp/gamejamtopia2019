﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerBackgroundDeslizante : MonoBehaviour
{

    [SerializeField] GamePlayController gamePlayController = null;

    [SerializeField] private Canvas canvas = null;
    [SerializeField] private Material material = null;
    [SerializeField] public float scrollSpeed;

    private Vector2 savedOffset;
    // Start is called before the first frame update
    void Start()
    {

        savedOffset = material.GetTextureOffset("_MainTex");

    }

    // Update is called once per frame
    private void FixedUpdate()
    //void Update()
    {
        if (canvas.enabled == true && gamePlayController.started == true)
        {
            float y = Repeat(Time.time * scrollSpeed, 1f);
            material.SetTextureOffset("_MainTex", new Vector2(0, -y + savedOffset.y));


        }
    }


    private float Repeat(float t, float length)
    {
        return Mathf.Clamp(t - Mathf.Floor(t / length) * length, 0.0f, length);
    }


    void OnDisable()
    {

        material.SetTextureOffset("_MainTex", savedOffset);

    }


}

